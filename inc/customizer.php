<?php
/**
 * bfu Theme Customizer.
 *
 * @package bfu
 */


/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function bfu_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
	$wp_customize->remove_section("background_image");
	$wp_customize->remove_section("static_front_page");
	$wp_customize->remove_section("colors");
	$wp_customize->remove_section("nav");



	/*******************************************
					Color scheme
	********************************************/

	// add the section to contain the settings
	$wp_customize->add_section( 'bfuSettings' , array(
    	'title' =>  'BFU Einstellungen'
	) );





	// main color ( site title, h1, h2, h4. h6, widget headings, nav links, footer headings )
	$txtcolors[] = array(
	    'slug'=>'nav_bg_color',
	    'default' => '#008000',
	    'label' => 'Nav Hintergrund'
	);

	$txtcolors[] = array(
		'slug'=>'nav_color',
		'default' => '#6efa05',
		'label' => 'Nav Farbe'
	);

	$txtcolors[] = array(
		'slug'=>'nav_hover_color',
		'default' => '#ff00ff',
		'label' => 'Nav Aktive Farbe'
	);

	$txtcolors[] = array(
		'slug'=>'font_color',
		'default' => '#000000',
		'label' => 'Text Farbe'
	);

	$txtcolors[] = array(
		'slug'=>'background_color',
		'default' => '#6EFA05',
		'label' => 'Hintergrund'
	);


	// add the settings and controls for each color
	foreach( $txtcolors as $txtcolor ) {

	    // SETTINGS
	    $wp_customize->add_setting(
	        $txtcolor['slug'], array(
	            'default' => $txtcolor['default'],
	            'type' => 'option',
	            'capability' =>  'edit_theme_options'
	        )
	    );

	     // CONTROLS
		$wp_customize->add_control(
	    	new WP_Customize_Color_Control(
		        $wp_customize,
		        $txtcolor['slug'],
		        array('label' => $txtcolor['label'],
		        'section' => 'bfuSettings',
		        'settings' => $txtcolor['slug'])
	    	)
		);
	}

	$wp_customize->add_setting('bfu_karte', array(
		'type' => 'option'
	) );
	$wp_customize->add_control(
       new WP_Customize_Image_Control(
           $wp_customize,
           'bfu_karte',
           array(
               'label'      => __( 'Hintergrund (Karte)', 'bfu' ),
               'section'    => 'bfuSettings',
               'settings'   => 'bfu_karte',
               'context'    => 'Background image for content'
           )
       )
   );


}
add_action( 'customize_register', 'bfu_customize_register' );

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function bfu_customize_preview_js() {
	wp_enqueue_script( 'bfu_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20151215', true );
}
add_action( 'customize_preview_init', 'bfu_customize_preview_js' );



function wptutsplus_customize_colors() {
 	/**********************
	text colors
	**********************/

	$nav_bg_color = (get_option( 'nav_bg_color' ) ? get_option( 'nav_bg_color' ) : '#008000');
	$nav_color = (get_option( 'nav_color' ) ? get_option( 'nav_color' ) : '#6efa05');
	$nav_hover_color = (get_option( 'nav_hover_color' ) ? get_option( 'nav_hover_color' ) : '#FF00FF');

	$font_color = (get_option( 'font_color' ) ? get_option( 'font_color' ) : '#000000');
	$bg_color = (get_option( 'background_color' ) ? get_option( 'background_color' ) : '#6EFA05');


	/****************************************
	styling
	****************************************/
	?>
	<style>

	#page{
		background-image: linear-gradient(
			rgba(255,255,255,0.5),
			<?php echo "#" . $bg_color; ?>

			);
	}

	.main-navigation{
		background-color:<?php echo $nav_bg_color; ?>;
	}

	.main-navigation ul ul{
		background-color:<?php echo $nav_bg_color; ?>;
	}

	.menu>li{
		background-color:<?php echo $nav_color; ?>;
	}

	.menu li:hover, .menu li:active{
		background-color:<?php echo $nav_hover_color; ?>;
	}

/* Normal Screen */
@media screen and (min-width: 37.5em) {
	.current-menu-item, .current_page_ancestor{
    background-color:<?php echo $nav_hover_color; ?> !important;
	}
}
/* Small Screen */
@media screen and (max-width: 37.5em) {
	.current-menu-item>a:first-child , .current-menu-ancestor>a:first-child{
		background-color:<?php echo $nav_hover_color; ?> !important;
	}
}

.slicknav_menu {
	background-color:<?php echo $nav_bg_color; ?>;
}

.slicknav_btn {
	background-color:<?php echo $nav_bg_color; ?>;
}

.slicknav_nav {
	background-color:<?php echo $nav_bg_color; ?>;
}

.slicknav_nav .slicknav_row:hover{
    background-color:<?php echo $nav_color; ?>
}

.slicknav_nav a:hover{
    background-color:<?php echo $nav_color; ?>;
}



	#main{
		color:<?php echo $font_color; ?>;
	}

	</style>

<?php


}
add_action( 'wp_head', 'wptutsplus_customize_colors' );

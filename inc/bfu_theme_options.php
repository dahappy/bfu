<?php

/* ------------------ */
/* theme options page */
/* ------------------ */

global $current_blog;
if($current_blog->blog_id == 1){
  add_action( 'admin_init', 'theme_options_init' );
  add_action( 'admin_menu', 'theme_options_add_page' );
}
// Einstellungen registrieren (http://codex.wordpress.org/Function_Reference/register_setting)
function theme_options_init(){
  if (!class_exists('customMetaBoxes')){
    require_once get_template_directory() . '/inc/bfu-custom-fields.php';
  }
	register_setting( 'theme_options', customMetaBoxes::getPrefix('theme_options'), 'bfu_validate_options' );
  
}

// Seite in der Dashboard-Navigation erstellen
function theme_options_add_page() {
	add_theme_page('Optionen', 'Optionen', 'edit_theme_options', 'theme-optionen', 'bfu_theme_options_page' ); // Seitentitel, Titel in der Navi, Berechtigung zum Editieren (http://codex.wordpress.org/Roles_and_Capabilities) , Slug, Funktion 
}

// Optionen-Seite erstellen
function bfu_theme_options_page() {
global $select_options, $radio_options;
if ( ! isset( $_REQUEST['settings-updated'] ) )
	$_REQUEST['settings-updated'] = false; ?>

<div class="wrap"> 
<?php screen_icon(); ?><h2>Theme-Optionen für BFU Theme</h2> 

<?php if ( false !== $_REQUEST['settings-updated'] ) : ?> 
<div class="updated fade">
	<p><strong>Einstellungen gespeichert!</strong></p>
</div>
<?php endif; ?>

  <form method="post" action="options.php">
	<?php settings_fields( 'theme_options' ); ?>
    <?php $options = get_option( customMetaBoxes::getPrefix('theme_options')); 

    ?>


    <table class="form-table options_table">
        <?php
          $bezirke = array(
              'mitte_' => 'Mitte:',
              'friedrichshain_' => 'Friedrichshain-Kreuzberg:',
              'pankow_' => 'Pankow',
              'charlottenburg_' => 'Charlottenburg-Wilmersdorf',
              'spandau_' => 'Spandau',
              'steglitz_' => 'Steglitz-Zehlendorf',
              'tempelhof_' => 'Tempelhof-Schöneberg',
              'neukölln_' => 'Neukölln',
              'treptow_' => 'Treptow-Köpenick',
              'marzahn_' => 'Marzahn-Hellersdorf',
              'lichtenberg_' => 'Lichtenberg',
              'reinickendorf_' => 'Reinickendorf'
            );
          //var_dump($options);
          foreach($bezirke as $key => $value){
            echo '<tr valign="top">' . "\n";
            printf('<th scope="row">%1$s</th>'  . "\n", $value);
            printf('<td><input id="%1$s" class="regular-text" type="text" name="%1$s" value="%2$s" /></td>' . "\n",
              OptionNames::getName($key . 'link'),
              esc_attr($options[$key . 'link']));
            
            printf('<td><input id="%1$s" type="color" size="10" name="%1$s" value="%2$s" /></td>' . "\n" ,
              OptionNames::getName($key . 'color'),
              //'');
              esc_attr($options[$key . 'color']));
            echo '</tr>' . "\n";
          }

          echo "</table>" . "\n";
        ?>
        
    
    <!-- submit -->
    <p class="submit"><input type="submit" class="button-primary" value="Einstellungen speichern" /></p>
  </form>
</div>
<?php }

// Strip HTML-Code:
// Hier kann definiert werden, ob HTML-Code in einem Eingabefeld 
// automatisch entfernt werden soll. Soll beispielsweise im 
// Copyright-Feld KEIN HTML-Code erlaubt werden, kommentiert die Zeile 
// unten wieder ein. http://codex.wordpress.org/Function_Reference/wp_filter_nohtml_kses
function bfu_validate_options( $input ) {
	// $input['copyright'] = wp_filter_nohtml_kses( $input['copyright'] );
	return $input;
}

?>
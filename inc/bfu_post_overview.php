<?php



/* Display custom column */
function display_posts_dates( $column, $post_id ) {
  //var_dump($column);
    if ($column == 'startTime'){
      bfu_custom_sorter::get_overview($post_id);
    }
    if ($column == 'endTime'){
      bfu_custom_sorter::get_overview($post_id,false);

        //var_dump(get_metadata("post",$post_id,customMetaBoxes::getPrefix('end_datetime'),true));
    }
}
add_action( 'manage_bfu_kinder_posts_custom_column' , 'display_posts_dates', 10, 2 );

/* Add custom column to post list */
function add_date_columns( $columns ) {
    unset($columns['date']);
    return array_merge( $columns,
        array( 'startTime' => __( 'Start Datum' ),
              'endTime' => __('End Datum')
              )
    );
}
add_filter( 'manage_bfu_kinder_posts_columns' , 'add_date_columns' );


function sortable_eventDate_column( $columns ) {
    $columns['endTime'] = 'endTime';
    $columns['startTime'] = 'startTime';

    //To make a column 'un-sortable' remove it from the array
    //unset($columns['date']);
    return $columns;
    echo "das ist auch ein test";
}
add_filter( 'manage_edit-bfu_kinder_sortable_columns', 'sortable_eventDate_column' );


function eventDate_orderby( $query ) {
    if( ! is_admin() ){
      return;
    }

    $orderby = $query->get( 'orderby');
    //var_dump($query);
    if( 'endTime' == $orderby ) {
      $query->set('meta_key',customMetaBoxes::getPrefix('end_datetime'));
      $query->set('orderby','meta_value');
    }elseif ('startTime' == $orderby) {
      $query->set('meta_key',customMetaBoxes::getPrefix('start_datetime'));
      $query->set('orderby','meta_value');
    }
}
add_action( 'pre_get_posts', 'eventDate_orderby' );

?>

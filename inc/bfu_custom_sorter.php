<?php
class bfu_custom_sorter{

  public static function get_loop_args(){
    $now = new DateTime("now",new DateTimeZone(get_option('timezone_string')));
    $date = new DateTime($now->format('Y-m-d'),new DateTimeZone(get_option('timezone_string')));
    $args=array(
      'post_type' => array('bfu_kinder', 'bfu_erwachsene' ),
      'meta_key' => 'bfu_end_datetime',
      'meta_value' => $date->getTimestamp(),
      'meta_compare' => '>=',
      'orderby' => 'meta_value',
      'order' => 'ASC',
      'posts_per_page' => 20
    );
    return $args;
  }

  public static function get_overview($post_id,$startTime = true){
    $now = new DateTime("now",new DateTimeZone(get_option('timezone_string')));
    if ($startTime){
      $startDate = new DateTime("now",new DateTimeZone(get_option('timezone_string')));
      $startDate->setTimestamp(get_metadata("post",$post_id,customMetaBoxes::getPrefix('start_datetime'),true));
      echo $startDate->format('d-m-Y');
    }else{
      $endDate = new DateTime("now",new DateTimeZone(get_option('timezone_string')));
      $endDate->setTimestamp(get_metadata("post",$post_id,customMetaBoxes::getPrefix('end_datetime'),true));
      if ($endDate->getTimestamp() >= $now->getTimestamp()){
        echo $endDate->format('d-m-Y');
      }else{
        echo "<span class=\"PostOverview_over\">" . $endDate->format('d-m-Y') . "</span>";
      }
    }
  }

}



 ?>

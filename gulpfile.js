var fs = require('fs');
var gulp = require('gulp');
var glob = require("glob");
var gutil = require( 'gulp-util' );
var ftp = require( 'vinyl-ftp' );
var vinylFilterSince = require('vinyl-filter-since');

var FTPconfigFile = './gulpfile.ftpconfig.json';
var FTPconfig = require(FTPconfigFile);

// helper function to build an FTP connection based on our configuration
function getFtpConnection() {
    return ftp.create({
        host: FTPconfig.ftp.hostname,
        port: FTPconfig.ftp.port,
        user: FTPconfig.ftp.user,
        password: FTPconfig.ftp.password,
        parallel: 5,
        log: gutil.log
    });
}

gulp.task('glob-test', function () {
  glob(gutil.env.f, {nocase:true ,base: '.', buffer: false}, function (er, files) {
    console.log(files);
  });
});


gulp.task('ftp',function(){
  var conn= getFtpConnection();
  if(gutil.env.f !== undefined){
    //Glob first because nocase:true makes problems
    glob(gutil.env.f, {nocase:true ,base: '.', buffer: false}, function (er, files) {
      console.log("Upload " + gutil.env.f);
      return gulp.src(files, { base: '.', buffer: false, nocase:false })
          .pipe( conn.dest( FTPconfig.ftp.remoteFolder ) )
          ;
    });

  }else if(gutil.env.all !== undefined){
    console.log("\x1b[41m" + "Upload All" + "\x1b[0m");
    return gulp.src(FTPconfig.files, { base: '.', buffer: false, nocase:false })
        .pipe( conn.dest( FTPconfig.ftp.remoteFolder ) )
        ;
  }else{
    console.log("\x1b[42;30m" + "Upload Newer" + "\x1b[0m");
    return gulp.src(FTPconfig.files, { base: '.', buffer: false, nocase:false })
        .pipe(vinylFilterSince(new Date(FTPconfig.since || 0)))
        .pipe( conn.dest( FTPconfig.ftp.remoteFolder ) )
        //.pipe( conn.newer( FTPconfig.ftp.remoteFolder ) ) // only upload newer files
        .on('end', function() {
            //console.info('Transfer complete\n');

            // Store timestamp
            FTPconfig.since = new Date();

            fs.writeFile(FTPconfigFile, JSON.stringify(FTPconfig, null, 4));
          });
  }
});

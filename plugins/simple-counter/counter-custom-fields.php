<?php
class counter_OptionNames {
	public static function getName($value){
	  return counter_metaBoxes::getPrefix('counter_options') . '[' . $value . ']';
	}
}


class counter_metaBoxes{
	private static $_prefix = "bfu_";

	public static function getBlogId($key){
		return substr($key,8);
	}

	public static function getPrefix($wert = ""){
		return self::$_prefix . $wert;
	}

	public function __construct($prefix = 'bfu'){
		if (strpos($prefix, "_") != strlen($prefix)-1){
			self::$_prefix = $prefix . "_";
		}

		add_action( 'add_meta_boxes', array( $this, 'add_metabox'  )        );
        add_action( 'save_post',      array( $this, 'save_metabox' ), 10, 2 );
	}

	/**
     * Adds the meta box.
     */
    public function add_metabox() {
        add_meta_box(
	    	self::getPrefix('event') ,      // Unique ID
	    	esc_html__( 'Veranstaltungs Infos', 'example' ),    // Title
	    	array( $this, 'render_metabox' ),   // Callback function
	    	array('bfu_kinder','bfu_erwachsene'), // Admin page (or post type)
	    	'side',         // Context
	    	'default'         // Priority
  			);

    }

    public function create_value($timestamp){
    	//24.08.2016 12:51
    	if (!empty($timestamp)){
    		//var d = $('#input').datetimepicker('getValue');
    	}
    }

    public function render_metabox( $object, $box ) {
        // Add nonce for security and authentication.
        //wp_nonce_field( 'custom_nonce_action', 'custom_nonce' );

        wp_nonce_field( basename( __FILE__ ), self::getPrefix('event_nonce') );

		$start_datetime_value;
		$end_datetime_value;
		$format = 'd.m.Y';
		$value = get_post_meta( $object->ID, self::getPrefix('start_datetime'), true );
		if (empty($value)){
			$start_datetime_value = new DateTime("now",new DateTimeZone(get_option('timezone_string')));
			$start_datetime_value = $start_datetime_value->format($format);
			//die(get_option('timezone_string'));
		}else{
			$start_datetime_value = new DateTime();
			$start_datetime_value->setTimestamp($value);
			$start_datetime_value = $start_datetime_value->format($format);
		}
		$value = get_post_meta( $object->ID, self::getPrefix('end_datetime'), true );
		if (empty($value)){
			$end_datetime_value = new DateTime("now",new DateTimeZone(get_option('timezone_string')));
			$end_datetime_value = $end_datetime_value->format($format);
		}else{
			$end_datetime_value = new DateTime();
			$end_datetime_value->setTimestamp($value);
			$end_datetime_value = $end_datetime_value->format($format);
		}


		ob_start();
		?>
	  	<p>
	    	<label for="%1$s">%2$s</label>
	    	<br />
	    	<input class="widefat" type="text" name="%1$s" id="%1$s" value="%3$s" size="30" />
	  	</p>
	  	<?php

	  		$html_ausgabe = ob_get_clean();

	  		printf($html_ausgabe, self::getPrefix('start_datetime'),
	  							translate( "Start Tag der Veranstaltung", 'bfu' ),
	  							$start_datetime_value);

	  		printf($html_ausgabe, self::getPrefix('end_datetime'),
	  							translate( "End Tag der Veranstaltung", 'bfu' ),
	  							$end_datetime_value
	  		);

	  		printf($html_ausgabe, self::getPrefix('ort'),
	  							translate( "Ort der Veranstaltung", 'bfu' ),
	  		esc_attr( get_post_meta( $object->ID, self::getPrefix('ort'), true ) )
	  		);

	  		printf($html_ausgabe, self::getPrefix('zeit'),
	  							translate( "Zeit der Veranstaltung", 'bfu' ),
	  							esc_attr( get_post_meta( $object->ID, self::getPrefix('zeit'), true ) )
	  		);

	  		printf($html_ausgabe, self::getPrefix('preis'),
	  							translate( "Preis der Veranstaltung", 'bfu' ),
	  		esc_attr( get_post_meta( $object->ID, self::getPrefix('preis'), true ) )
	  		);




		?>
	  	<script type="text/javascript">
	  		jQuery('document').ready(function(){
				jQuery("#<?php echo self::getPrefix('start_datetime') ?>").datepicker({
					dateFormat: "dd.mm.yy"
				});
				jQuery("#<?php echo self::getPrefix('end_datetime') ?>").datepicker({
					dateFormat: "dd.mm.yy"
				});
			});

		</script>

		<?php

    }

    /**
     * Handles saving the meta box.
     *
     * @param int     $post_id Post ID.
     * @param WP_Post $post    Post object.
     * @return null
     */
    public function save_metabox( $post_id, $post ) {

    	//die(var_dump($_POST));


        // Add nonce for security and authentication.
        $nonce_name   = isset( $_POST[self::getPrefix('event_nonce')] ) ? $_POST[$this->getPrefix('event_nonce')] : '';
        $nonce_action = basename( __FILE__ );

        // Check if nonce is set.
        if ( ! isset( $nonce_name ) ) {
            return;
        }

        // Check if nonce is valid.
        if ( ! wp_verify_nonce( $nonce_name, $nonce_action ) ) {
            return;
        }

        // Check if user has permissions to save data.
        if ( ! current_user_can( 'edit_post', $post_id ) ) {
            return;
        }

        // Multisite Check
        if ( is_multisite() && ms_is_switched() )
      		return $post_id;

        // Check if not an autosave.
        if ( wp_is_post_autosave( $post_id ) ) {
            return;
        }

        // Check if not a revision.
        if ( wp_is_post_revision( $post_id ) ) {
            return;
        }

        $keys = array(
        		self::getPrefix('start_datetime'),
        		self::getPrefix('end_datetime'),
        		self::getPrefix('zeit'),
        		self::getPrefix('ort'),
        		self::getPrefix('preis')
        );

		//30.03.2005 01:57

        $convert = DateTime::createFromFormat('d.m.Y',$_POST[self::getPrefix('start_datetime')]);
        $_POST[self::getPrefix('start_datetime')] = $convert->format('U');


        $convert = DateTime::createFromFormat('d.m.Y',$_POST[self::getPrefix('end_datetime')]);
		$_POST[self::getPrefix('end_datetime')] = $convert->format('U');


        foreach ($keys as $key){
        	/* Get the posted data and sanitize it for use as an HTML class. */
			$new_meta_value = ( isset( $_POST[$key] ) ? esc_attr( $_POST[$key] ) : '' );

			/* Get the meta key. */
			$meta_key = $key;

			/* Get the meta value of the custom field key. */
			$meta_value = get_post_meta( $post_id, $meta_key, true );

			/* If a new meta value was added and there was no previous value, add it. */
			if ( $new_meta_value && '' == $meta_value )
				add_post_meta( $post_id, $meta_key, $new_meta_value, true );

			/* If the new meta value does not match the old value, update it. */
			elseif ( $new_meta_value && $new_meta_value != $meta_value )
				update_post_meta( $post_id, $meta_key, $new_meta_value );

			/* If there is no new meta value but an old value exists, delete it. */
			elseif ( '' == $new_meta_value && $meta_value )
				delete_post_meta( $post_id, $meta_key, $meta_value );
        }


    }


} // Class End


?>

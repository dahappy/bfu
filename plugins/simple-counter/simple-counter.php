<?php
/*
Plugin Name: Simple Counter
Plugin URI:
Description: Simple Counter for BFU Site
Version: 1.0
Author: daHappy (Heiko Werner)
Author URI: https://www.dahappy.de
*/

require_once plugin_dir_path( __FILE__ ) . '/counter-custom-fields.php';
require_once plugin_dir_path( __FILE__) . '/simple-counter-widget.php';

add_action( 'admin_menu', 'counter_options_add_page' );
add_action( 'network_admin_menu', 'counter_options_add_page' );
/*add_action(
    'network_admin_edit_bfu_counter_options',
    'save_counter_options'
);*/
register_activation_hook( __FILE__, 'table_creation' );
// register widget
add_action('widgets_init', create_function('', 'return register_widget("simple_counter");'));


function table_creation(){

    global $wpdb;
    $wpdb->show_errors();
    $table_name = $wpdb->base_prefix . "happy_counter";
    $charset_collate = $wpdb->get_charset_collate();

    $sql = "CREATE TABLE $table_name (
      id mediumint(9) NOT NULL AUTO_INCREMENT,
      time datetime DEFAULT CURRENT_TIMESTAMP NOT NULL,
      ip varchar(15) NOT NULL,
      blog_id mediumint(9) NOT NULL,
      PRIMARY KEY  (id)
    ) $charset_collate;";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );
}

// Seite in der Dashboard-Navigation erstellen
function counter_options_add_page() {
    $user = wp_get_current_user();
    if ( $user->has_cap( 'manage_options' ) ) {
           add_menu_page('Counter', 'Counter Optionen', 'manage_options', 'bfu_counter_options', 'bfu_counter_options_page' ); // Seitentitel, Titel in der Navi, Berechtigung zum Editieren (http://codex.wordpress.org/Roles_and_Capabilities) , Slug, Funktion
       }
}

// Optionen-Seite erstellen
function bfu_counter_options_page() {
global $select_options, $radio_options,$current_blog;
if ( ! isset( $_REQUEST['settings-updated'] ) )
	$_REQUEST['settings-updated'] = false; ?>

<div class="wrap">
<?php screen_icon(); ?><h2>Counter-Optionen für BFU Theme</h2>


<?php 
if (isset($_POST[counter_metaBoxes::getPrefix('counter_settings_nonce')])){
    $nonce_name   = isset( $_POST[counter_metaBoxes::getPrefix('counter_settings_nonce')] ) ? $_POST[counter_metaBoxes::getPrefix('counter_settings_nonce')] : '';
    $nonce_action = basename( __FILE__ );

    $error = false;
    // Check if nonce is set.
    if ( ! isset( $nonce_name ) ) {
       $error = "Interner Fehler 1";
    }

    // Check if nonce is valid.
    if ($error == false && !wp_verify_nonce( $nonce_name, $nonce_action ) ) {
        $error = "Interner Fehler 2";
    }

    foreach($_POST['bfu_counter_options'] as $key=>$value){
      $_POST['bfu_counter_options'][$key] == trim($_POST['bfu_counter_options'][$key]);
      if ($error == false && !is_numeric(trim($value))){
        if($key == "timeout"){
          $error = "Der Wert für TimeOut ist keine Zahl";
        }else{
          foreach(wp_get_sites() as $site){
            if ($site['blog_id'] == counter_metaBoxes::getBlogId($key)){
              $error = "Der Wert für " . $site['path'] . "ist keine Zahl";      
              break;
            }         
          }  
        }

      }else{
        $_POST['bfu_counter_options'][$key] == trim($value);
      }
    }

    if(!$error){
      update_site_option('bfu_counter_options',$_POST['bfu_counter_options']);  
      echo '<div class="updated fade">';
      echo "\t<p><strong>Einstellungen gespeichert!</strong></p>";
      echo "</div>";
    }else{
      echo '<div class="notice error">';
      echo "\t<p><strong>$error</strong></p>";
      echo "</div>";
    }  
}
  
  printf('<form method="post" action="%1$s">','admin.php?page=bfu_counter_options');
  
  
  $options=get_site_option('bfu_counter_options');

  wp_nonce_field( basename( __FILE__ ), counter_metaBoxes::getPrefix('counter_settings_nonce') );
  ?>

    <table class="form-table options_table">
        <tr valign="top">
            <th scope="row">TimeOut (in Min)</th>
        <?php
          printf('<td><input id="%1$s" class="regular-text" type="text" name="%1$s" value="%2$s" style="width:5em;"/></td>' . "\n",
              counter_OptionNames::getName('timeout'),
              esc_attr(isset($options['timeout']) ? $options['timeout'] : 60));
        ?>
        </tr>
        <?php

            foreach(wp_get_sites() as $site){
                if ($site['public'] == 1){
                    echo "<tr valign=\"top\">\n";
                    echo "\t<th scope=\"row\">Count " . $site['path'] . "</th>\n";
                    printf("\t" . '<td><input id="%1$s" class="regular-text" type="text" name="%1$s" value="%2$s" style="width:5em;"/></td>' . "\n",
                        counter_OptionNames::getName('count_id' . $site['blog_id']),
                        esc_attr(isset($options['count_id' . $site['blog_id']]) ?
                        $options['count_id' . $site['blog_id']] : 0));
                }
            }
         ?>
    </table>

    <!-- submit -->
    <p class="submit"><input type="submit" class="button-primary" value="Einstellungen speichern" /></p>
  </form>
</div>


<?php 
}

?>

<?php
global $wpdb;
$wpdb->show_errors();
define( 'DIEONDBERROR', true );

class simple_counter extends WP_Widget {

	// constructor
	public function __construct() {
		$widget_ops = array(
			'classname' => 'simple_counter',
			'description' => 'Simple Counter by daHappy(Heiko Werner)');
		parent::__construct( 'simple_counter', 'Simple Counter', $widget_ops );
	}

	// widget form creation
	function form($instance) {
		if( $instance) {
		     $text = esc_attr($instance['text']);
		} else {
		     $text = '';
		}
	?>
		<p>
			<label for="<?php echo $this->get_field_id('text'); ?>"><?php _e('PreText:', 'wp_widget_plugin'); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id('text'); ?>" name="<?php echo $this->get_field_name('text'); ?>" type="text" value="<?php echo $text; ?>" />
		</p>
	<?php
	}

	// widget update
	function update($new_instance, $old_instance) {
		$instance = $old_instance;
		// Fields

		$instance['text'] = strip_tags($new_instance['text']);
		return $instance;
	}

	// widget display
	function widget($args, $instance) {
		extract( $args );
		// these are the widget options

		$text = $instance['text'];
		echo $before_widget;
		// Display the widget

		echo "<div class=\"simple_counter_plugin_text\">";

		// Check if text is set
		if( $text ) {
	    	echo $text;
		}
			echo self::counter_get_value();

		echo "</div>";
		echo $after_widget;
	}

	public static function counter_get_value(){
		global $wpdb;
		global $current_blog;
		$options=get_site_option('bfu_counter_options');
		//var_dump($options);
		
	    $table_name = $wpdb->base_prefix . "happy_counter";

		$now = new DateTime("now",new DateTimeZone(get_option('timezone_string')));
		$now = $now->sub(new DateInterval('PT' . $options['timeout']*60 . 'S'));
		$wpdb->query("DELETE FROM $table_name WHERE time<='" . $now->format('Y-m-d H:i:s') . "'");

		$ip_user = $_SERVER['REMOTE_ADDR'];
		$table_entrys = $wpdb->get_results("SELECT * FROM $table_name WHERE ip='$ip_user'");

/*if (isset($table_entrys)){
	var_dump($table_entrys);
	die ("Ausgabe==>" . $wpdb->num_rows . "<==");	
}*/
//var_dump($wpdb->num_rows);

		if ($wpdb->num_rows==0){
			//IP noch nicht eingetragen
			$wpdb->insert($table_name,array('ip'=>$ip_user,
											'blog_id'=>$current_blog->blog_id));
			$options['count_id' . $current_blog->blog_id]+=1;
			if ($current_blog->blog_id != 1){
				$wpdb->insert($table_name,array('ip'=>$ip_user,
												'blog_id'=>1));
				$options['count_id1']+=1;
			}
		}elseif ($wpdb->num_rows==1){
			//IP bereits eingetragen aber nur Main Blog
			if ($current_blog->blog_id != 1){
				$wpdb->insert($table_name,array('ip'=>$ip_user,
											'blog_id'=>$current_blog->blog_id));
				$options['count_id' . $current_blog->blog_id]+=1;

			}
		}else{
			//IP bereits eingetragen auf MainBlog und mindestens einem SubBlog
			$treffer = false;
			foreach($table_entrys as $table_row){
				if ($table_row->blog_id == $current_blog->blog_id){
					$treffer = true;
				}
			}
			if (!$treffer){
				$wpdb->insert($table_name,array('ip'=>$ip_user,
											'blog_id'=>$current_blog->blog_id));
				$options['count_id' . $current_blog->blog_id]+=1;
			}
		}
		update_site_option('bfu_counter_options',$options);

		//Update to current Time
		$now = new DateTime("now",new DateTimeZone(get_option('timezone_string')));
		$wpdb->update(
			$table_name,
			array("time"=>$now->format('Y-m-d H:i:s')),
			array('ip'=>$ip_user)
		);

		return $options['count_id' . $current_blog->blog_id];
		
	}//function END
}//class end



?>

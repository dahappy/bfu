<?php
/*
Plugin Name: Customizer Cleanup
Plugin URI: 
Description: Ändert die Customizer einstellungen
Version: 1.0
Author: daHappy (Heiko Werner)
Author URI: https://www.dahappy.de
*/






/**
 * Removes the core 'Menus' panel from the Customizer.
 *
 * @param array $components Core Customizer components list.
 * @return array (Maybe) modified components list.
 */

function wpdocs_remove_nav_menus_panel( $components ) {
    $i = array_search( 'nav_menus', $components );
    if ( false !== $i ) {
        unset( $components[ $i ] );
    }
    return $components;
}
add_filter( 'customize_loaded_components', 'wpdocs_remove_nav_menus_panel' );




?>
<?php
/**
 * bfu functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package bfu
 */

if ( ! function_exists( 'bfu_setup' ) ) :


/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function bfu_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on bfu, use a find and replace
	 * to change 'bfu' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'bfu', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'bfu' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

/*
	add_theme_support( 'custom-logo', array(
		'height'      => 184,
		'width'       => 177,
		'flex-height' => true,
		'flex-width'  => true,
		'header-text' => array( 'site-title', 'site-description' ),
	) );


	// Add theme support for Custom Background
	add_theme_support( 'custom-background', array(
		'default-image'          => get_template_directory() . '/img/TK_Karte.png',
	) );
*/
	require get_template_directory() . '/inc/bfu-custom-fields.php';
	require_once (get_template_directory() . '/inc/bfu_post_overview.php');
	require_once get_template_directory() . '/inc/bfu_custom_sorter.php';

}
endif;
add_action( 'after_setup_theme', 'bfu_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function bfu_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'bfu_content_width', 640 );
}
add_action( 'after_setup_theme', 'bfu_content_width', 0 );

//##################################################

//$current_site = get_current_site();
//var_dump($current_site);
global $current_blog;
//$blog_id = $current_blog->blog_id;
//var_dump($post);
//die(var_dump($current_blog));

//##################################################

add_action( 'widgets_init', 'cwwp_unregister_default_widgets' );
/**
 * Unregisters the default widgets in WordPress. Tested with 3.5-alpha.
 * Simply uncomment specific widgets to unregister them.
 *
 * @since 1.0.0
 */
function cwwp_unregister_default_widgets() {

	unregister_widget( 'WP_Widget_Archives' );
	unregister_widget( 'WP_Widget_Calendar' );
	unregister_widget( 'WP_Widget_Categories' );
	unregister_widget( 'WP_Nav_Menu_Widget' );
	unregister_widget( 'WP_Widget_Links' );
	unregister_widget( 'WP_Widget_Meta' );
	unregister_widget( 'WP_Widget_Pages' );
	unregister_widget( 'WP_Widget_Recent_Comments' );
	unregister_widget( 'WP_Widget_Recent_Posts' );
	unregister_widget( 'WP_Widget_RSS' );
	unregister_widget( 'WP_Widget_Search' );
	unregister_widget( 'WP_Widget_Tag_Cloud' );
	unregister_widget( 'WP_Widget_Text' );

}

//##################################################
add_action( 'plugins_loaded', 'cwwp_remove_default_widgets_action' );
/**
 * Removes all default widgets before they have a chance to get registered.
 *
 * Method is future proof for automatically removing any new default
 * widgets that WordPress may add.
 *
 * @since 1.0.0
 */
function cwwp_remove_default_widgets_action() {
	remove_action( 'init', 'wp_widgets_init', 1 );

}
add_action( 'init', 'cwwp_do_widgets_init', 1 );
/**
 * Adds back the widgets_init hook.
 *
 * @since 1.0.0
 */
function cwwp_do_widgets_init() {
	do_action( 'widgets_init' );

}


/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */

function bfu_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'bfu' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'bfu' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'bfu_widgets_init' );





/*Simpliefy Backend*/
function bfu_remove_dashboard_widgets() {
    $user = wp_get_current_user();
    if ( ! $user->has_cap( 'manage_options' ) ) {
        remove_meta_box( 'dashboard_activity', 'dashboard', 'normal' );
        remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
        remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );
        add_meta_box('bfu_dashboard_welcome', 'Willkommen', 'bfu_links_dashboard', 'dashboard' ,'side');
        wp_add_dashboard_widget( 'bfu_dashboard_welcome', 'Links', 'bfu_links_dashboard','side' );
    }
}
add_action( 'wp_dashboard_setup', 'bfu_remove_dashboard_widgets' );

function bfu_links_dashboard() { ?>
    Wichtige Links die euch helfen können:
    <ul>
        <li><a href="https://bitbucket.org/dahappy/bfu/wiki/">Das Wiki</a></li>
        <li><a href="https://bitbucket.org/dahappy/bfu/issues/">Fehlermeldungen</a></li>
    </ul>
<?php }



function bfu_changeBackend(){
	$user = wp_get_current_user();
    if ( ! $user->has_cap( 'manage_options' ) ) {
        // remove_menu_page( 'edit.php' );
        remove_menu_page( 'edit-comments.php' );
        remove_menu_page( 'edit.php?post_type=page' );
        remove_menu_page( 'tools.php' );
    }
}
add_action( 'admin_menu', 'bfu_changeBackend' );


function bfu_changeToolbar($wp_admin_bar){
	$user = wp_get_current_user();
    if ( ! $user->has_cap( 'manage_options' ) ) {
    	$wp_admin_bar->remove_node( 'my-sites' );
        $wp_admin_bar->remove_node( 'comments' );
        // $wp_admin_bar->remove_node( 'new-post' );
        $wp_admin_bar->remove_node( 'new-page' );
        $wp_admin_bar->remove_node( 'edit' );
        $wp_admin_bar->remove_node( 'view' );

    }
}
add_action( 'admin_bar_menu', 'bfu_changeToolbar', 999 );

function register_posts(){
  /*register_post_type( 'bfu_dauer',
    array(
      'labels' => array(
        'name' => __( 'Dauertipps' ),
        'singular_name' => __( 'Dauertipp' )
	  ),
	  'public' => true,
      'has_archive' => true,
      'menu_position' =>5,
	  'supports' => array('title','editor')

  ));
	*/
  register_post_type( 'bfu_kinder',
    array(
      'labels' => array(
        'name' => __( 'Veranstaltungen Kinder' ),
        'singular_name' => __( 'Veranstaltung Kinder' )
      ),
      'public' => true,
      'has_archive' => true,
      'menu_position' =>5,
	  'supports' => array('title','editor')
    )
  );

  register_post_type( 'bfu_erwachsene',
    array(
      'labels' => array(
        'name' => __( 'Veranstaltungen Erwachsene' ),
        'singular_name' => __( 'Veranstaltung Erwachsene' )
      ),
      'public' => true,
      'has_archive' => true,
      'menu_position' =>5,
      'supports' => array('title','editor')
    )
  );
}
add_action( 'init', 'register_posts' );


function meta_boxes_setup(){
	new customMetaBoxes();

}
/* Fire our meta box setup function on the post editor screen. */
add_action( 'load-post.php', 'meta_boxes_setup' );
add_action( 'load-post-new.php', 'meta_boxes_setup' );



/**
 * Enqueue scripts and styles.
 */
function bfu_scripts() {
	//wp_enqueue_script( 'bfu-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );
	wp_enqueue_script( 'bfu-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );
	//################################## Eigenes ###########################
	wp_enqueue_style( 'bfu-style', get_stylesheet_uri() );
	wp_enqueue_style('font','https://fonts.googleapis.com/css?family=Caveat+Brush');
	wp_enqueue_script( 'jquery' );
	wp_enqueue_script('SlickNav',get_template_directory_uri() . '/vendor/SlickNav/jquery.slicknav.min.js',array('jquery'));
	wp_enqueue_style('SlickNav',get_template_directory_uri() . '/vendor/SlickNav/slicknav.min.css');
	wp_enqueue_script('SlickNav Init',get_template_directory_uri() . '/js/slicknav_init.js');

	if(is_page_template('main-root.php')){
		wp_enqueue_style('daHappy_main-rot_style', get_template_directory_uri() . '/main-root_style.css');
		wp_enqueue_script( 'leaflet', get_template_directory_uri() .
			'/js/leaflet/leaflet-src.js');
		wp_enqueue_style( 'leaflet', get_template_directory_uri() . '/js/leaflet/leaflet.css');
	}else{
		wp_enqueue_style('daHappy_style', get_template_directory_uri() . '/daHappy_style.css');
	}


	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'bfu_scripts' );

/*

*/
function bfu_admin_scripts(){
	if (is_admin()){
		wp_enqueue_script( 'jquery' );
		wp_enqueue_script('jquery-ui-core');
		wp_enqueue_script('jquery-ui-datepicker');
		wp_enqueue_style('jQuery UI',get_template_directory_uri() .
			'/layouts/jquery/jquery-ui.min.css');

		wp_enqueue_script('ColorPicker JS',get_template_directory_uri() .
			'/js/colorpicker.js');
		wp_enqueue_style('ColorPicker CSS',get_template_directory_uri() .
			'/js/colorpicker.css');
		wp_enqueue_style('Backend CSS', get_template_directory_uri() .
			'/daHappy_backend.css');
		wp_enqueue_script('DateTimePicker JS',get_template_directory_uri() .
				'/js/datetimepicker/jquery.datetimepicker.full.min.js');
		wp_enqueue_style('DateTimePicker CSS',get_template_directory_uri() .
				'/js/datetimepicker/jquery.datetimepicker.min.css');
	}

	//edit-bfu_kinder
	//die(var_dump(get_current_screen()));
	if (get_current_screen()->id == 'edit-bfu_kinder'){
		wp_enqueue_script('teint old posts',get_template_directory_uri() .
			'/js/bfu_date_teint.js');
	}

}
add_action( 'admin_enqueue_scripts', 'bfu_admin_scripts' );



require_once( get_template_directory() . '/inc/bfu_theme_options.php');

/**
 * Implement the Custom Header feature.
 */
// require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package bfu
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer site_footer_all" role="contentinfo">
		<div class="site-info">
			<a href="<?php echo esc_url( __( 'http://www.system-data.de/', 'bfu' ) ); ?>"><?php printf( esc_html__( 'Proudly powered by %s', 'BFU' ), 'System-Data' ); ?></a>
			<span class="sep"> | </span>
			<?php printf( esc_html__( 'Theme: %1$s by %2$s.', 'BFU' ), '<a href="https://bitbucket.org/dahappy/bfu/overview" rel="theme">BFU</a>', '<a href="http://www.dahappy.de" rel="designer">daHappy (Heiko Werner)</a>' ); ?>
			<span style="float:right; margin-right:5px">
				<a href="<?php echo get_permalink(get_page_by_path('Impressum')); ?>">Impressum</a>
			</span>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>

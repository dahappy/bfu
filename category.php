<?php
/**
 * The template for displaying all Category posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#category
 *
 * @package bfu
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php
		$category = get_category( get_query_var( 'cat' ) );
		$cat_id = $category->cat_ID;
		$args = array ( 'category' => $cat_id);
		$myposts = get_posts( $args );
		foreach( $myposts as $post ) :	setup_postdata($post);

			get_template_part( 'template-parts/content-category', get_post_format() );

			//the_post_navigation();

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endforeach; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();

<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package bfu
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
    <!--<div id="colophon1" class="site-footer"></div>-->
    <a class="skip-link screen-reader-text" href="#main"><?php esc_html_e( 'Skip to content', 'bfu' ); ?></a>
    <header id="masthead" class="site-header" role="banner">
        <div class="site-branding">

        <?php if ( get_header_image() ) : ?>

        <?php else : ?>

        <?php endif; ?>


            <?php
            $header_color = (get_option( 'background_color' ) ? get_option( 'background_color' ) : '6EFA05');
            if (substr($header_color,0,1) == "#"){
              $header_color = substr($header_color,1);
            }
            printf('<a href="%1s" rel="home" style="background-image:url(\'%2s\')">',
            esc_url( home_url( '/' ) ),
            get_template_directory_uri() . "/header_image.php?color=" . $header_color
            );
            echo "</a>";
            $description = get_bloginfo( 'description', 'display' );
            if ( $description || is_customize_preview() ) : ?>
                <p class="site-description"><?php echo $description; /* WPCS: xss ok. */ ?></p>
            <?php
            endif; ?>
        </div><!-- .site-branding -->

        <nav id="site-navigation" class="main-navigation" role="navigation">
            <!-- <button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php //esc_html_e( 'Primary Menu', 'bfu' ); ?>
 -->               <!--  <img src="<?php echo get_template_directory_uri() ?>/img/hamenu.png" /> -->
            <!-- </button> -->
            <?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
            <?php dynamic_sidebar( 'sidebar-1' ); ?>
        </nav><!-- #site-navigation -->
        <style>
            #main::after{
                background:url('<?php echo get_option('bfu_karte');?>');
                background-position:50% 0;
                background-repeat:no-repeat;
                background-size:contain;
                content:"";
                opacity:0.1;
                top:0;
                left:0;
                bottom:0;
                right:0;
                z-index:-1;
            }
        </style>
    </header><!-- #masthead -->



    <div id="content" class="site-content">

<?php
$img = imagecreatefrompng("img/header.png");

if (isset($_GET['color'])){

    imagetruecolortopalette($img,false, 255);

    $target[0] = hexdec(substr($_GET['color'],0,2));
    $target[1] = hexdec(substr($_GET['color'],2,2));
    $target[2] = hexdec(substr($_GET['color'],4,2));

    $bg = imagecolorclosest($img,255,0,0); //RED Transparent
    imagecolortransparent($img,$bg);

    $index = imagecolorclosest ( $img,  0,255,0 ); //Get the green
    imagecolorset($img,$index,$target[0],$target[1],$target[2]); // SET NEW COLOR


}

header("Content-type: image/png");
imagepng($img);
imagedestroy($img);

//imagepng($img,"test.png");

?>

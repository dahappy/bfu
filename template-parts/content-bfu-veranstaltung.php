<?php
/**
 * Template part for displaying BFU Veranstaltungen.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package bfu
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>">

	<header class="entry-header">
		<div class="bfu_event_date">
		<?php
			$weekDays = array(
				__("So",'bfu'),
				__("Mo",'bfu'),
				__("Di",'bfu'),
				__("Mi",'bfu'),
				__("Do",'bfu'),
				__("Fr",'bfu'),
				__("Sa",'bfu')
				);

			$start_datetime = DateTime::createFromFormat('U',
				get_metadata("post",get_the_ID(),customMetaBoxes::getPrefix('start_datetime'),true)
			);

			$end_datetime = DateTime::createFromFormat('U',
				get_metadata("post",get_the_ID(),customMetaBoxes::getPrefix('end_datetime'),true)
			);


			echo $weekDays[$start_datetime->format('w')];
			if($end_datetime->getTimestamp() <= $start_datetime->getTimestamp()){
				// Zeiten werden als GLEICH betrachtet (Endzeit ignore)
				echo '<br>' . $start_datetime->format('j.n.');
			}else{
				//zeiten werden als UNGLEICH betrachtet
				echo " " . $start_datetime->format('j.n.') . "<br>";
				echo "bis " . $weekDays[$end_datetime->format('w')] . "<br>";
				echo $end_datetime->format('j.n.');

			}

			//die(var_dump($start_datetime));
		?>
		</div>
		<?php
			the_title( '<div class="entry-title">', '</div>' );
		?>
	</header><!-- .entry-header -->

	<div class="entry-content" style="background-color:none;">
		<?php
			the_content( sprintf(
				/* translators: %s: Name of current post. */
				wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'bfu' ), array( 'span' => array( 'class' => array() ) ) ),
				the_title( '<span class="screen-reader-text">"', '"</span>', false )
			) );

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'bfu' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php
			$ort = get_metadata("post",get_the_ID(),customMetaBoxes::getPrefix('ort'),true);
			$zeit = get_metadata("post",get_the_ID(),customMetaBoxes::getPrefix('zeit'),true);
			$preis = get_metadata("post",get_the_ID(),customMetaBoxes::getPrefix('preis'),true);
			if (!empty($ort)){
				echo '<div class="event_wrapper ort_wrapper">';
				echo '<div class="event_label ort_label">Ort:</div>';
				printf('<div class="event_value ort_value"> %1$s</div>',$ort);
				echo '</div>';
			}
			if (!empty($zeit)){
				echo '<div class="event_wrapper zeit_wrapper">';
				echo '<div class="event_label zeit_label">Zeit:</div>';
				printf('<div class="event_value zeit_value" >%1$s</div>',$zeit);
				echo '</div>';
			}
			if (!empty($preis)){
				echo '<div class="event_wrapper preis_wrapper">';
				echo '<div class="event_label preis_label">Preis:</div>';
				printf('<div class="event_value preis_value"> %1$s</div>',$preis);
				echo '</div>';
			}else{
				echo '<div class="event_wrapper preis_wrapper">';
				echo '<div class="event_value preis_value">Eintritt frei</div>';
				echo '</div>';
			}

		?>
		<!--
		<div class="ort_label">Ort:</div>
		<div class="ort_value"></div>

		<div class="zeit_label">Zeit:</div>
		<div class="zeit_value"></div>

		<div class="preis_label">Preis:</div>
		<div class="preis_value"></div>

		<!--<?php bfu_entry_footer(); ?>-->
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->

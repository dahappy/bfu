<?php
/*
Template Name: z_MAIN-ROOT -dont use-
*/


?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>


</head>
<body>
<div id="page" class="site">
	<header id="masthead" class="site-header" role="banner">

		<div class="site-branding">

            <?php if ( get_header_image() ) : ?>
						<?php endif; // End header image check.?>
				<div class="site-branding_pic" style="background-image:url('<?php echo get_template_directory_uri() . "/img/Berlin_fast_umsonst_root.png"; ?>')">
				</div>

			<?php
			while ( have_posts() ) : the_post();
				get_template_part( 'template-parts/content', get_post_format() );
			endwhile;
			?>

        </div><!-- .site-branding -->

        <nav id="site-navigation" class="main-navigation" role="navigation">
            <?php dynamic_sidebar( 'sidebar-1' ); ?>
        </nav><!-- #site-navigation -->
    </header><!-- #masthead -->


<div id="content" class="site-content">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div id="map"></div>





		<script type="text/javascript">

		var mymap = L.map("map",{
			//center: [52.522906, 13.408813],
			//zoom:10,
			attributionControl:false,
			//maxZoom: 10,
			//minZoom: 10,
			zoomControl: false,
			boxZoom: false,
			touchZoom:false,
			scrollWheelZoom:false,
			dragging: false,
			doubleClickZoom: false,
			zoomSnap: 0.25

		});


	var bezirke = L.geoJson(null,{onEachFeature:own_style}).addTo(mymap);
	// var bezirke = L.geoJson().addTo(mymap);
	jQuery.ajax({url:"<?php echo get_template_directory_uri(); ?>/js/json/bezirke_small2.json", dataType:"json", cache:false})
		.done(function( data ) {
			bezirke.addData(data);
			bezirke.setStyle({color:'black'});
			var southWest  = bezirke.getBounds().getSouthWest();
			var northEast  = bezirke.getBounds().getNorthEast();
			var bounds = new L.LatLngBounds(southWest, northEast);
			mymap.fitBounds(bounds);
	});



	<?php
		require_once get_template_directory() . '/inc/bfu-custom-fields.php';
		$options = get_option( customMetaBoxes::getPrefix('theme_options'));

		$bezirke = array(
              'mitte_' => 'Mitte',
              'friedrichshain_' => 'Friedrichshain-Kreuzberg',
              'pankow_' => 'Pankow',
              'charlottenburg_' => 'Charlottenburg-Wilmersdorf',
              'spandau_' => 'Spandau',
              'steglitz_' => 'Steglitz-Zehlendorf',
              'tempelhof_' => 'Tempelhof-Schöneberg',
              'neukölln_' => 'Neukölln',
              'treptow_' => 'Treptow-Köpenick',
              'marzahn_' => 'Marzahn-Hellersdorf',
              'lichtenberg_' => 'Lichtenberg',
              'reinickendorf_' => 'Reinickendorf'
            );

		echo 'function highlight_feature(e){' . "\n";
		echo "\t" . 'switch (e.target.feature.properties.name){' . "\n";

		foreach ($bezirke as $key=>$value){
			if ($options[$key . 'color'] != '#000000' &&
				$options[$key . 'color'] != '#ffffff'){
				printf("\t" . 'case "%1$s":' . "\n", $value);
				printf("\t\t" .
					'e.target.setStyle({fillColor:"%1$s",fillOpacity:"1", clickable:true});'.
					"\n", $options[$key . 'color']);
				printf("\t\t" . 'jQuery("#info").text("%1$s");' . "\n", $value);
				echo "\t\tbreak; \n";
			}
		}
	?>
			default:
				e.target.setStyle({fillColor:'white',fillOpacity:'1'});
				break;
		}

	}

	function feature_click(e){
		switch(e.target.feature.properties.name){
			<?php
				foreach ($bezirke as $key=>$value){
					if ($options[$key . 'color'] != '#000000' &&
						$options[$key . 'color'] != '#ffffff'){
						printf("\t" . 'case "%1$s":' . "\n", $value);
						printf("\t\t" . 'window.document.location.href = "%1$s";' . "\n",
							str_replace("baseurl;",get_bloginfo('url'),$options[$key . 'link']));
						echo "\t\tbreak;\n";
					}
				}
			?>
		}
	}

	function resetHighlight(e){
		e.target.setStyle({fillColor:'white'});
		jQuery('#info').text("");
	}

	function own_style(feature,layer){
		layer.setStyle({weight:'2'});

		layer.on({	mouseover:highlight_feature,
					mouseout:resetHighlight
				});

		if (<?php

			ob_start();
			foreach ($bezirke as $key=>$value){
				if ($options[$key . 'color'] != '#000000' &&
					$options[$key . 'color'] != '#ffffff'){
					printf('feature.properties.name != "%1$s" &&' . "\n", $value);
				}
			}
			$ausgabe = ob_get_clean();
			$ausgabe = substr($ausgabe,0,strlen($ausgabe)-3);
			echo $ausgabe;

		?>){
			layer.setStyle({interactive:false});
			layer.setStyle({fillColor:'lightgray',fillOpacity:1});

		}else{
			layer.on({click:feature_click});
			layer.setStyle({fillColor:'white',fillOpacity:1});
		}

	}
	jQuery('.entry-content').css("opacity","0");
	jQuery('.entry-content').animate({
		opacity: 1
	}, 5000, function(){

	});



</script>




		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();

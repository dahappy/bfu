<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package bfu
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}

$trace = debug_backtrace();

$from_index = false;
if (isset($trace[0])) {
    $file = basename($trace[3]['file']);

    echo $file;
}
?>



<aside id="secondary" class="widget-area" role="complementary">
    <div class="dTipp_header">
        <div class="dTipp_header wrapper">

          <script>
            jQuery(document).ready(function(){

              jQuery( "#datepicker" ).calendario({displayWeekAbbr:true,
                weekabbrs : [ 'So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa' ]});
            });

          </script>
          <div id="datepicker" class="fc-calendar-container"></div>


          <div class="img_wrapper" style="background-image:url('<?php echo get_option('dt_baer') ?>')">
          <!-- TK_Tipp || Ness_edit -->

            <!--<img src="<?php echo get_template_directory_uri();?>/img/TK_Tipp.jpg">-->
          </div>
        </div>
        <div class="dTipp_head_fill"></div>
        <div class="dTipp_head_txt" onclick="head_click()">
            DIE DAUER TIPPS
        </div>
    </div> <!-- .dTipp_header -->
    <div class="dTipp_content_wrapper">
    <div class="dTipp_content">

        <?php $query = new WP_Query( array('post_type' => 'bfu_dauer') ); ?>
        <?php if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>


             <div class="dTipp_post_head">
                <?php the_title(); ?>
             </div>

              <div class="dTipp_post_content">
                <?php the_content(); ?>
              </div>

         <?php endwhile;
         wp_reset_postdata();
         else : ?>
            <div class="dTipp_post_head">
                Entschuldigung aber es sind noch keine DauerTipps vorhanden
            </div>
         <?php endif; ?>


    </div>
    </div>

</aside><!-- #secondary -->
